#ifndef ROBOT_IO_MOTOR_CONTROLLER_H_
#define ROBOT_IO_MOTOR_CONTROLLER_H_

#include "robot_io/PID.h"

#include <ros/ros.h>
#include <robot_io_msgs/Encoders.h>
#include <robot_io_msgs/MotorCommands.h>
#include <geometry_msgs/Twist.h>
#include <robot_io/PIDConfig.h>
#include <dynamic_reconfigure/server.h>

template<typename T>
struct LeftRightPair
{
  T l;
  T r;

  LeftRightPair(T l_, T r_) : l(l_), r(r_) {};
  LeftRightPair() : l(T(0)), r(T(0)) {};
};

class RobotIO;

class MotorController
{
  protected:
    ros::NodeHandle nh, pnh;
    ros::Subscriber vel_cmds_sub;
    ros::Publisher est_vel_pub;

    typedef robot_io::PIDConfig Config;
    typedef dynamic_reconfigure::Server<Config> ReconfigServer;
    std::shared_ptr<ReconfigServer> l_server, r_server;

    PID l_pid, r_pid;

    bool active_vel_pub;
    ros::Time last_vel_cmd;

    bool tuning, l_cfg_init, r_cfg_init;
    double wheel_base_diameter, tics_per_rad;
    double feed_fwd_m, feed_fwd_b, acceleration_threshold;
    int min_cmd, max_cmd;
    LeftRightPair<double> ang_vel, ang_vel_cmd, ff_cmd;
    LeftRightPair<double> wheel_radius, last_mean;

    void velCmdsCB(const geometry_msgs::Twist::ConstPtr&);
    void cfgCB(Config&, uint32_t, PID*, bool*);

  public:
    MotorController(ros::NodeHandle, ros::NodeHandle);

    void estimateVelocity(const robot_io_msgs::Encoders&);
    robot_io_msgs::MotorCommands computeControlInputs();

  friend class RobotIO;
};

#endif
