#ifndef ROBOT_IO_PID_H_
#define ROBOT_IO_PID_H_

#include <ros/ros.h>
#include <math.h>

template <typename T> int sgn(T val) {
  return (T(0) < val) - (val < T(0));
}

class PID
{
  public:
    double integral_threshold, previous_error, error_sum, kp, ki, kd;
    
    PID() : kp(0), ki(0), kd(0) {};
    PID(double kp_, double ki_, double kd_, double it_) :
      kp(kp_), 
      ki(ki_), 
      kd(kd_), 
      integral_threshold(it_), 
      previous_error(0),
      error_sum(0)
    {
    }        

    double operator()(double error)
    {
      error_sum += error;

      // apply threshold
      if (fabs(error_sum) > integral_threshold)
          error_sum = sgn(error_sum)*integral_threshold;

      double error_diff = error - previous_error;
      previous_error = error;

      //ROS_DEBUG("PID: e = %.2f, e_sum = %.2f, e_diff = %.2f",
      //        error, error_sum, error_diff);
      //ROS_DEBUG("PID: p = %.3f, i = %.3f, d = %.3f",
      //    kp*error, ki*error_sum, kd*error_diff);

      return kp*error + ki*error_sum + kd*error_diff;
    }
};

#endif
