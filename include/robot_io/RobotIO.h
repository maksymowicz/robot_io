#ifndef ROBOT_IO_ROBOT_IO_H_
#define ROBOT_IO_ROBOT_IO_H_

#include "robot_io/MotorController.h"

#include <ros/ros.h>
#include <robot_io_msgs/MotorCommands.h>

class RobotIO
{
  protected:
    int fd;
    bool active_motor_cmd_pub;
    ros::Time last_motor_cmd;

    MotorController mc;

    ros::NodeHandle nh, pnh;
    ros::Publisher tic_pub;
    ros::Subscriber motor_cmd_sub;

    robot_io_msgs::MotorCommands motor_commands;

  public:
    RobotIO(ros::NodeHandle, ros::NodeHandle);
    ~RobotIO();
    int initSerialPort(const char*, int);
    void readEncoderTics();
    void sendMotorCmds();
    void motorCmdsCB(const robot_io_msgs::MotorCommands::ConstPtr&);
    void run();
};

#endif
