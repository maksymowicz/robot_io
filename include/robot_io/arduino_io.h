#ifndef ROBOT_IO_ARDUINO_IO_H_
#define ROBOT_IO_ARDUINO_IO_H_

#include <ros/ros.h>
#include <robot_io_msgs/MotorCommands.h>
#include <geometry_msgs/Twist.h>
#include <robot_io/PIDConfig.h>
#include <dynamic_reconfigure/server.h>

class ArduinoIO
{
  private:
    int fd;

    ros::NodeHandle nh, pnh;
    ros::Publisher tics_pub, est_vel_pub;
    ros::Subscriber motor_cmd_sub, vel_cmd_sub;

    typedef robot_io::PIDConfig Config;
    typedef dynamic_reconfigure::Server<Config> ReconfigServer;
    std::shared_ptr<ReconfigServer> server;

    bool cfg_init;
    double l_wheel_radius, r_wheel_radius, wheel_base_diameter, tics_per_rev;
    double feed_fwd_m, feed_fwd_b, motor_min, motor_max, acceleration_threshold;

    void readBytes(char*, int);

  public:
    //ArduinoIO();
    ArduinoIO(ros::NodeHandle, ros::NodeHandle);
    ~ArduinoIO();
    int initSerialPort(const char*, int);
    void readArduinoMsg();
    void sendCtrlParams(Config&, uint32_t, bool*);
    void sendVelocityCmds(const geometry_msgs::Twist::ConstPtr&);
    void sendMotorCmds(const robot_io_msgs::MotorCommands::ConstPtr&);
    void run();
};

#endif
