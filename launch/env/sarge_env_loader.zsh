#!/usr/bin/env zsh

source ~/catkin_ws/devel/setup.zsh
export ROS_MASTER_URI=http://192.168.1.150:11311
export ROS_IP=$(hostname -I)

exec "$@"
