#include "robot_io/MotorController.h"

#include <ros/console.h>
#include <math.h>

using namespace std;
namespace rc = ros::console;

MotorController::MotorController(ros::NodeHandle nh_, ros::NodeHandle pnh_) : 
  nh(nh_),
  pnh(pnh_),
  tuning(false),
  l_cfg_init(false),
  r_cfg_init(false)
{
  // verbosity
  bool debug;
  if (pnh.getParam("debug_verbosity", debug) && debug) {
    rc::set_logger_level(ROSCONSOLE_DEFAULT_NAME, rc::levels::Debug);
    rc::notifyLoggerLevelsChanged();
  }

  if (pnh.getParam("tuning_mode", tuning) && tuning)
    est_vel_pub = nh.advertise<geometry_msgs::Twist>("est_vel", 10);

  // set dynamic reconfigure callback
  auto l_fcn = bind(&MotorController::cfgCB, this, _1, _2, &l_pid, &l_cfg_init);
  auto r_fcn = bind(&MotorController::cfgCB, this, _1, _2, &r_pid, &r_cfg_init);
  l_server.reset(new ReconfigServer(ros::NodeHandle("~/left")));
  r_server.reset(new ReconfigServer(ros::NodeHandle("~/right")));
  l_server->setCallback(l_fcn);
  r_server->setCallback(r_fcn);
  while (!l_cfg_init || !r_cfg_init) ros::Rate(100).sleep();

  // robot parameters
  double tics_per_rev;
  string params_prefix;
  if (!pnh.getParam("params_prefix", params_prefix))
    ROS_WARN("MotorController::MotorController(): global param prefix "
            "not set");
  ros::NodeHandle gnh('/' + params_prefix.c_str());
  if (!gnh.getParam("wheels/r_radius", wheel_radius.l) ||
    !gnh.getParam("wheels/l_radius", wheel_radius.r) ||
    !gnh.getParam("wheels/wheel_base_diameter", wheel_base_diameter) ||
    !gnh.getParam("wheels/tics_per_revolution", tics_per_rev) ||
    !pnh.getParam("feed_forward/m", feed_fwd_m) ||
    !pnh.getParam("feed_forward/b", feed_fwd_b) ||
    !pnh.getParam("motor_bounds/min", min_cmd) ||
    !pnh.getParam("motor_bounds/max", max_cmd) ||
    !pnh.getParam("acceleration_threshold", acceleration_threshold)) {
    ROS_FATAL("MotorController::MotorController(): unable to read robot "
            "parameters from server");
    exit(EXIT_FAILURE);
  }
  tics_per_rad = tics_per_rev / (2.0 * M_PI);

  // velocity commands subscriber
  vel_cmds_sub = nh.subscribe("velocity_commands", 30,
      &MotorController::velCmdsCB, this);
}

void MotorController::cfgCB(Config& cfg, uint32_t level, PID* pid, bool* init)
{
  pid->kp = cfg.kp;
  pid->ki = cfg.ki;
  pid->kd = cfg.kd;
  pid->integral_threshold = cfg.integral_threshold;

  *init = true;
}

void MotorController::estimateVelocity(const robot_io_msgs::Encoders &tics1)
{
  static const int len = 5;
  static int index = 0;
  static double l_hist[len] = {0};
  static double r_hist[len] = {0};
  static robot_io_msgs::Encoders tics0 = tics1;

  //ROS_DEBUG("Encoder msg: left = %d, right = %d, millis = %d",
  //        tics1.left, tics1.right, tics1.millis);

  int dt = tics1.millis - tics0.millis; // ms
  if (dt == 0) return;

  // estimate wheel angular velocity in tics/s
  LeftRightPair<double> hist_val(l_hist[index], r_hist[index]);
  l_hist[index] = (tics1.left  - tics0.left )*1000.0 / ((double)(dt*len));
  r_hist[index] = (tics1.right - tics0.right)*1000.0 / ((double)(dt*len));
  ang_vel.l += l_hist[index] - hist_val.l;
  ang_vel.r += r_hist[index] - hist_val.r;

  //ROS_DEBUG("ang_vel.l = %.3f, ang_vel.r = %.3f", ang_vel.l, ang_vel.r);

  index = ++index % len;
  tics0 = tics1;

  if (tuning) {
    geometry_msgs::Twist est_vel;
    est_vel.linear.x = 0.5/tics_per_rad*(ang_vel.l*wheel_radius.l +
        ang_vel.r*wheel_radius.r);
    est_vel.angular.z = 1.0/(tics_per_rad*wheel_base_diameter) *
        (ang_vel.r*wheel_radius.r - ang_vel.l*wheel_radius.l);
    est_vel_pub.publish(est_vel);
  }
}

robot_io_msgs::MotorCommands MotorController::computeControlInputs()
{
  robot_io_msgs::MotorCommands cmd;
  cmd.left =  ff_cmd.l + l_pid(ang_vel_cmd.l - ang_vel.l);
  cmd.right = ff_cmd.r + r_pid(ang_vel_cmd.r - ang_vel.r);

  // lower and upper bound the motor commands
  if (abs(cmd.left) > max_cmd) {
    int old = cmd.left;
    cmd.left = sgn(cmd.left)*max_cmd;
    ROS_WARN("Left motor input saturated:  %d -> %d", old, cmd.left);
  }
  if (abs(cmd.right) > max_cmd) {
    int old = cmd.right;
    cmd.right = sgn(cmd.right)*max_cmd;
    ROS_WARN("Right motor input saturated: %d -> %d", old, cmd.right);
  }
  if (abs(cmd.left) < min_cmd)
      cmd.left = 0;
  if (abs(cmd.right) < min_cmd)
      cmd.right = 0;

  //ROS_DEBUG("motor inputs: left = %d, right = %d", cmd.left, cmd.right);

  return cmd;
}

void MotorController::velCmdsCB(const geometry_msgs::Twist::ConstPtr& msg)
{
  static geometry_msgs::Twist vel;
  vel = *msg;

  // keep robot from performing a wheelie by enforcing an acceleration threshold
  double v_c = 0.5/tics_per_rad*(ang_vel.l*wheel_radius.l
      + ang_vel.r*wheel_radius.r);
  if (vel.linear.x - v_c > acceleration_threshold) {
    vel.linear.x = v_c + acceleration_threshold;
    //ROS_DEBUG("Applied threshold: v_in = %.3f, v_c = %.3f, v_out = %.3f", 
    //    msg->linear.x, v_c, vel.linear.x);
  }

  active_vel_pub = true;
  last_vel_cmd = ros::Time::now();

  // compute left and right motor velocity setpoints (tics/s)
  LeftRightPair<double> lin_vel;
  lin_vel.l = vel.linear.x - 0.5*vel.angular.z*wheel_base_diameter;
  lin_vel.r = vel.linear.x + 0.5*vel.angular.z*wheel_base_diameter;
  ang_vel_cmd.l = tics_per_rad/wheel_radius.l * lin_vel.l;
  ang_vel_cmd.r = tics_per_rad/wheel_radius.r * lin_vel.r;

  // compute feed forward input from estimated model
  ff_cmd.l = lin_vel.l*feed_fwd_m + sgn(lin_vel.l)*feed_fwd_b;
  ff_cmd.r = lin_vel.r*feed_fwd_m + sgn(lin_vel.r)*feed_fwd_b;
}
