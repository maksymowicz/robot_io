#include <ros/ros.h>
#include <rosbag/bag.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <mutex>
#include <string>

using namespace message_filters;
using namespace sensor_msgs;
using namespace std;
using namespace cv;

typedef sync_policies::ApproximateTime<Image, Image> Policy;
typedef Image::ConstPtr ImgPtr;

ImgPtr ex_color, ex_depth;
mutex image_mutex;

void imageCb(const ImgPtr& depth, const ImgPtr& color)
{
  lock_guard<mutex> lock(image_mutex);
  ex_color = color;
  ex_depth = depth;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "extrinsic_calibration_data_logging_node");
  ros::NodeHandle nh;

  Subscriber<sensor_msgs::Image> depth_sub, color_sub;
  depth_sub.subscribe(nh, "depth", 3);
  color_sub.subscribe(nh, "color", 3);

  Synchronizer<Policy> img_sync(Policy(3), depth_sub, color_sub);
  img_sync.registerCallback(bind(imageCb, _1, _2));

  ros::AsyncSpinner spinner(2);
  spinner.start();
  ros::Rate loop_rate(1); // 1 second
  ROS_INFO("Allowing camera to stabilize for 5s");
  for (int i = 0; i < 5; ++i)
    loop_rate.sleep();

  rosbag::Bag bag;
  bag.open("extrinsic_frames.bag", rosbag::bagmode::Write);

  string const color_topic("/xtion/rgb/image_rect_color");
  string const depth_topic("/xtion/depth_registered/image_raw");
  sensor_msgs::Image color_img, depth_img;
  {
    lock_guard<mutex> lock(image_mutex);
    if (ex_color && ex_depth) {
      color_img = *ex_color;
      depth_img = *ex_depth;
      bag.write(color_topic.c_str(), ros::Time::now(), ex_color);
      bag.write(depth_topic.c_str(), ros::Time::now(), ex_depth);
    } else {
      if (!ex_color)
        ROS_FATAL("No color image");
      if (!ex_depth)
        ROS_FATAL("No depth image");
      return -1;
    }
  }
  ROS_INFO("Saved images to extrinsic_frames.bag");
  bag.close();

  cv_bridge::CvImagePtr im1, im2;
  try {
    im1 = cv_bridge::toCvCopy(color_img, image_encodings::BGR8);
    im2 = cv_bridge::toCvCopy(depth_img, image_encodings::TYPE_16UC1);
  } catch (cv_bridge::Exception& e) {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return -1;
  }

  imshow("color", im1->image);
  waitKey(0);

  double min;
  double max;
  minMaxIdx(im2->image, &min, &max);
  Mat adjMap;
  const double alpha = 255.0 / (max - min);
  const double beta = alpha * min;
  convertScaleAbs(im2->image, adjMap, alpha, beta);
  imshow("depth", adjMap);
  waitKey(0);

  return 0;
}
