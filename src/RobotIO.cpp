#include "robot_io/RobotIO.h"

#include <ros/console.h>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <cstdint>
#include <fcntl.h>

using namespace std;
namespace rc = ros::console;

uint16_t fletcher16(uint8_t const *data, size_t bytes)
{
  uint16_t sum1 = 0xff, sum2 = 0xff;
  size_t tlen;

  while (bytes) {
    tlen = ((bytes >= 20) ? 20 : bytes);
    bytes -= tlen;
    do {
      sum2 += sum1 += *data++;
      tlen--;
    } while (tlen);
    sum1 = (sum1 & 0xff) + (sum1 >> 8);
    sum2 = (sum2 & 0xff) + (sum2 >> 8);
  }

  sum1 = (sum1 & 0xff) + (sum1 >> 8);
  sum2 = (sum2 & 0xff) + (sum2 >> 8);
  return (sum2 << 8) | sum1;
}

RobotIO::RobotIO(ros::NodeHandle nh_, ros::NodeHandle pnh_) : 
  mc(nh_, pnh_), nh(nh_), pnh(pnh_)
{
  string serial_port;
  if (!pnh.getParam("serial_io/port", serial_port)) {
    ROS_FATAL("Serial port required");
    exit(EXIT_FAILURE);
  }

  int baud_rate;
  if (!pnh.getParam("serial_io/baud_rate", baud_rate)) {
    baud_rate = 57600;
    ROS_WARN("No baud_rate specified");
    ROS_WARN("Using default value of %d", baud_rate);
  }

  bool debug;
  if (pnh.getParam("debug_verbosity", debug) && debug) {
    rc::set_logger_level(ROSCONSOLE_DEFAULT_NAME, rc::levels::Debug);
    rc::notifyLoggerLevelsChanged();
  }

  tic_pub = nh.advertise<robot_io_msgs::Encoders>("tics", 30);
  motor_cmd_sub = nh.subscribe("motor_cmds", 30, &RobotIO::motorCmdsCB, this);

  const char *port = serial_port.c_str();
  if (initSerialPort(port, baud_rate) == -1)
    exit(EXIT_FAILURE);
}

RobotIO::~RobotIO()
{
  // stop the robot
  int16_t buf[2] = {0, 0}; // motor commands need to be 2 bytes each
  buf[0] = 0;
  buf[1] = 0;
  uint16_t checksum = fletcher16((uint8_t *)buf, 4);
  write(fd, (void *)buf, 4);
  write(fd, &checksum, 2);

  close(fd);
}

void RobotIO::motorCmdsCB(const robot_io_msgs::MotorCommands::ConstPtr& cmd)
{
  motor_commands = *cmd;
  active_motor_cmd_pub = true;
  last_motor_cmd = ros::Time::now();
}

int RobotIO::initSerialPort(const char* port, int rate)
{
  // open serial port for reading and writing, don't make it the controlling 
  // terminal, and wait for the arduino to be ready before returning
  fd = open(port, O_RDWR | O_NOCTTY);
  if (fd == -1) {
    ROS_FATAL("Unable to open serial port %s", port);
    return -1;
  }

  // retrieve current terminal settings and save a copy
  struct termios newio;
  if (tcgetattr(fd, &newio) < 0) {
    ROS_FATAL("Unable to retrieve settings for port %s", port);
    return -1;
  }
  
  /* control options */
  newio.c_cflag |= CREAD | CLOCAL; // enable reading & ignore control lines
  newio.c_cflag &= ~CRTSCTS; // no hardware flow control

  // 8N1 input
  newio.c_cflag &= ~PARENB;  // no parity bit
  newio.c_cflag &= ~CSTOPB;  // 1 stop bit
  newio.c_cflag &= ~CSIZE;   // clear data bit size
  newio.c_cflag |= CS8;      // 8 data bits

  /* local options */
  newio.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // raw input

  /* input options */
  newio.c_iflag &= ~(IXON | IXOFF | IXANY); // no software flow control

  /* output options */
  newio.c_oflag &= ~OPOST; // raw output

  /* control character options */
  newio.c_cc[VMIN] = 0;
  newio.c_cc[VTIME] = 0;
  
  // set data rates
  speed_t brate;
  switch (rate) {
    case 4800:   brate=B4800;   break;
    case 9600:   brate=B9600;   break;
    case 19200:  brate=B19200;  break;
    case 38400:  brate=B38400;  break;
    case 57600:  brate=B57600;  break;
    case 115200: brate=B115200; break;
    default: ROS_WARN("Invalid baud_rate");
             ROS_WARN("Using default value of 57600");
             brate = B57600;
             break;
  }
  cfsetispeed(&newio, brate);
  cfsetospeed(&newio, brate);

  // set port attributes
  if (tcsetattr(fd, TCSANOW, &newio) < 0) {
    ROS_FATAL("Unable to configure attributes on port %s", port);
    return -1;
  }

  usleep(2000 * 1000);
  tcflush(fd, TCIOFLUSH);
  //ROS_DEBUG("Opened serial port %s", port);
  usleep(1500 * 1000); // pause to allow for arduino to reboot

  return 0;
}

void RobotIO::readEncoderTics()
{
  // read 14 bytes from the serial buffer
  // messages are: l_tics, r_tics, millis - all 4 byte longs - and a
  // 2 byte checksum
  static const int buf_len = 14;
  char buf[buf_len];
  char b;
  int i = 0;
  int timeout = 5000;
  while (i < buf_len) {
    int n = read(fd, &b, 1);
    if (n == -1) {
      ROS_FATAL("RobotIO::readEncoderTics(): Error reading serial port");
      return;
    }
    if (n == 0) {
      usleep(1000);
      timeout--;
      if (timeout == 0) {
        ROS_FATAL("RobotIO::readEncoderTics(): Timed out while reading port");
        return;
      }
      continue;
    }
    buf[i] = b;
    ++i;
  }

  // reform longs from char array
  robot_io_msgs::Encoders tics;
  tics.left   = buf[0] | (buf[1] << 8) | (buf[2]  << 16) | (buf[3]  << 24);
  tics.right  = buf[4] | (buf[5] << 8) | (buf[6]  << 16) | (buf[7]  << 24);
  tics.millis = buf[8] | (buf[9] << 8) | (buf[10] << 16) | (buf[11] << 24);
  tics.stamp = ros::Time::now();

  // check for errors using checksum
  uint16_t msg_checksum = buf[12] | (buf[13] << 8);
  uint16_t checksum = fletcher16((uint8_t *)buf, 12);
  if (msg_checksum != checksum) {
    //ROS_DEBUG("l = %d, r = %d, t = %d", tics.left, tics.right, tics.millis);
    //ROS_DEBUG("msg_checksum = %d, checksum = %d", msg_checksum, checksum);
    return;
  }

  // publish tics and update velocity estimates
  tic_pub.publish(tics);
  mc.estimateVelocity(tics);
}

void RobotIO::sendMotorCmds()
{
  // give preference to velocity commands
  static int16_t buf[2] = {0, 0}; // motor commands need to be 2 bytes each
  if (mc.active_vel_pub) {
    robot_io_msgs::MotorCommands cmd = mc.computeControlInputs();
    buf[0] = cmd.left;
    buf[1] = cmd.right;
    if ((ros::Time::now() - mc.last_vel_cmd).toSec() > 1.0)
      mc.active_vel_pub = false;
  } else if (active_motor_cmd_pub) { 
    buf[0] = motor_commands.left;
    buf[1] = motor_commands.right;
    if ((ros::Time::now() - last_motor_cmd).toSec() > 1.0)
      active_motor_cmd_pub = false;
  } else { // decelerate robot if no new messages have been received recently
    buf[0] *= 0.975;
    buf[1] *= 0.975;
    //ROS_DEBUG("Cmd message to arduino: %d, %d", buf[0], buf[1]);
  }

  // write motor commands
  uint16_t checksum = fletcher16((uint8_t *)buf, 4);
  int num = write(fd, (void *)buf, 4);
  int check = write(fd, &checksum, 2);
  if (num + check != 6)
    ROS_WARN("RobotIO::sendMotorCmds.cpp(): Error sending data over serial "
        "port: tried to send 6 bytes but only %d were sent", num+check);
}

void RobotIO::run()
{
  ros::Rate loop_rate(30);
  while (ros::ok()) {
    readEncoderTics();
    sendMotorCmds();
    ros::spinOnce();
    loop_rate.sleep();
  }
}
