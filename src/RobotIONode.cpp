#include "robot_io/RobotIO.h"

int main(int argc, char** argv)
{
  ros::init(argc, argv, "robot_io_node");
  ros::NodeHandle nh, pnh("~");
  RobotIO robot_io(nh, pnh);

  robot_io.run();

  return 0;
}
