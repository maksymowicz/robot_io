#include <ros/ros.h>
#include <ros/console.h>
#include <geometry_msgs/Twist.h>
#include <robot_io_msgs/MotorCommands.h>
#include <signal.h>
#include <termios.h>
#include <stdio.h>
#include <thread>
#include <mutex>

#define KEYCODE_R 0x43 
#define KEYCODE_L 0x44
#define KEYCODE_U 0x41
#define KEYCODE_D 0x42
#define KEYCODE_Q 0x71

using namespace std;
namespace rc = ros::console;

class Teleop
{
  public:
    void keyLoop();
    Teleop();

  private: 
    double linear_increment, angular_increment;
    int linear, angular;
    string cmd_type;
    ros::NodeHandle nh, pnh;
    ros::Publisher cmd_pub;

    void velocityCmdPublisher();
    void motorCmdPublisher();
    thread publisher_thread;
    mutex msg_mutex;
};

Teleop::Teleop() : 
  pnh("~"),
  linear(0),
  angular(0)
{
  bool debug;
  if (pnh.getParam("debug", debug) && debug) { 
    rc::set_logger_level(ROSCONSOLE_DEFAULT_NAME, rc::levels::Debug);
    rc::notifyLoggerLevelsChanged();
  }

  if (pnh.getParam("linear_increment", linear_increment) && 
      pnh.getParam("angular_increment", angular_increment) && 
      pnh.getParam("command_type", cmd_type)) {
    ROS_INFO("linear_increment: \t%f", linear_increment);
    ROS_INFO("angular_increment: \t%f", angular_increment);
    ROS_INFO("command_type: \t%s", cmd_type.c_str());
    cout << endl;
  } else {
    ROS_FATAL("Failed to read parameters from server");
    exit(EXIT_FAILURE);
  }

  if (cmd_type.compare("velocity") == 0) {
    cmd_pub = nh.advertise<geometry_msgs::Twist>("velocity_commands", 1);
    ROS_INFO("Sending %s commands to the robot", cmd_type.c_str());
    ROS_INFO("---------------------------");
    ROS_INFO("Use arrow keys to move the robot.");
    cout << endl;
    publisher_thread = thread(&Teleop::velocityCmdPublisher, this);
  } else if(cmd_type.compare("motor") == 0) {
    cmd_pub = nh.advertise<robot_io_msgs::MotorCommands>("motor_commands", 1);
    ROS_INFO("Sending %s commands to the robot", cmd_type.c_str());
    ROS_INFO("---------------------------");
    ROS_INFO("Use arrow keys to move the robot.");
    cout << endl;
    publisher_thread = thread(&Teleop::motorCmdPublisher, this);
  } else {
    ROS_ERROR("Unrecognized command type: %s", cmd_type.c_str());
    ROS_ERROR("Options are: velocity, motor");
    exit(EXIT_FAILURE);
  }
}

void Teleop::velocityCmdPublisher()
{
  ros::Rate loop_rate(10);
  while (ros::ok()) {
    geometry_msgs::Twist msg;
    {
      lock_guard<mutex> lock(msg_mutex);
      msg.linear.x = linear*linear_increment;
      msg.angular.z = angular*angular_increment;
    }

    cout << "\r\33[2K\033[A\33[2K";
    cout << "Linear velocity : \t" << msg.linear.x << endl;
    cout << "Angular velocity : \t" << msg.angular.z << flush;

    static int stop_pub_count = 0;
    if (!(msg.linear.x == 0.0 && msg.angular.z == 0.0)) {
      cmd_pub.publish(msg);
      stop_pub_count = 0;
    }
    else if (stop_pub_count == 0) {
      cmd_pub.publish(msg);
      ++stop_pub_count;
    }
    loop_rate.sleep();
  }
}

void Teleop::motorCmdPublisher()
{
  ros::Rate loop_rate(10);
  while (ros::ok()) {
    robot_io_msgs::MotorCommands msg;
    {
      lock_guard<mutex> lock(msg_mutex);
      msg.left  = linear*linear_increment - angular*angular_increment;
      msg.right = linear*linear_increment + angular*angular_increment;
    }

    cout << "\r\33[2K\033[A\33[2K";
    cout << "Left motor command : \t" << msg.left << endl;
    cout << "Right motor command : \t" << msg.right << flush;

    cmd_pub.publish(msg);
    loop_rate.sleep();
  }
}

int kfd;
struct termios cooked, raw;

void quit(int sig)
{
  tcsetattr(kfd, TCSANOW, &cooked);
  ros::shutdown();
  exit(0);
}

void Teleop::keyLoop()
{
  tcgetattr(kfd, &cooked);
  memcpy(&raw, &cooked, sizeof(struct termios));
  raw.c_lflag &=~ (ICANON | ECHO);
  raw.c_cc[VEOL] = 1;
  raw.c_cc[VEOF] = 2;
  tcsetattr(kfd, TCSANOW, &raw);

  char c;
  while (ros::ok()) {
    if (read(kfd, &c, 1) < 0) {
      ROS_FATAL("Error reading from keyboard");
      exit(EXIT_FAILURE);
    }

    ROS_DEBUG("value: 0x%02X\n", c);

    lock_guard<mutex> lock(msg_mutex);
    switch (c) {
      case KEYCODE_L: ROS_DEBUG("LEFT");  ++angular; break;
      case KEYCODE_R: ROS_DEBUG("RIGHT"); --angular; break;
      case KEYCODE_U: ROS_DEBUG("UP");    ++linear;  break;
      case KEYCODE_D: ROS_DEBUG("DOWN");  --linear;  break;
      case 's': ROS_DEBUG("s"); linear = angular = 0; break;
      default: continue;
    }
  }

  return;
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "teleop_TIM");
  Teleop teleop;

  signal(SIGINT, quit);

  teleop.keyLoop();

  return 0;
}
