#include "robot_io/arduino_io.h"

#include <ros/console.h>
#include <robot_io_msgs/Encoders.h>

#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <cstdint>
#include <fcntl.h>

using namespace std;

template <typename T> T sgn(T val) {
  return (T(0) < val) - (val < T(0));
}

union Float13 {
  char c[52];
  float f[13];
};

union Float2 {
  char c[8];
  float f[2];
};

union Int2 {
  char c[4];
  int16_t i[2];
};

union Long3 {
  char c[12];
  int32_t l[3];
};

union Checksum {
  char c[2];
  uint16_t i;
};

uint16_t fletcher16(uint8_t const *data, size_t bytes)
{
  uint16_t sum1 = 0xff, sum2 = 0xff;
  size_t tlen;

  while (bytes) {
    tlen = ((bytes >= 20) ? 20 : bytes);
    bytes -= tlen;
    do {
      sum2 += sum1 += *data++;
      tlen--;
    } while (tlen);
    sum1 = (sum1 & 0xff) + (sum1 >> 8);
    sum2 = (sum2 & 0xff) + (sum2 >> 8);
  }

  sum1 = (sum1 & 0xff) + (sum1 >> 8);
  sum2 = (sum2 & 0xff) + (sum2 >> 8);
  return (sum2 << 8) | sum1;
}

void ArduinoIO::readBytes(char* buf, int bytes)
{
  char b;
  int i = 0;
  int timeout = 1000;
  while (i < bytes) {
    int n = read(fd, &b, 1);
    if (n == -1) {
      ROS_FATAL("readBytes(): Error reading serial port");
      exit(EXIT_FAILURE);
    }
    if (n == 0) {
      usleep(1000);
      timeout--;
      if (timeout == 0) {
        ROS_FATAL("readBytes(): Timed out while reading port");
        exit(EXIT_FAILURE);
      }
      continue;
    }
    buf[i] = b;
    ++i;
  }
}

/*
ArduinoIO::ArduinoIO()
{
  string serial_port = "/dev/robotIO";
  if (initSerialPort(serial_port.c_str(), 57600) == -1)
    exit(EXIT_FAILURE);
}
*/

ArduinoIO::ArduinoIO(ros::NodeHandle nh_, ros::NodeHandle pnh_) : 
  nh(nh_), pnh(pnh_), cfg_init(false)
{
  //
  // start serial communication with arduino
  //

  string serial_port;
  if (!pnh.getParam("serial_io/port", serial_port)) {
    ROS_FATAL("Serial port required");
    exit(EXIT_FAILURE);
  }

  int baud_rate;
  if (!pnh.getParam("serial_io/baud_rate", baud_rate)) {
    baud_rate = 57600;
    ROS_WARN("No baud_rate specified");
    ROS_WARN("Using default value of %d", baud_rate);
  }

  const char *port = serial_port.c_str();
  if (initSerialPort(port, baud_rate) == -1)
    exit(EXIT_FAILURE);

  //
  // load robot parameters
  //

  string params_prefix;
  bool res = pnh.getParam("params_prefix", params_prefix);
  if (!res)
    ROS_WARN("ArduinoIO::ArduinoIO(): global param prefix not set");
  ros::NodeHandle gnh(std::string("/") + params_prefix);
  if (!gnh.getParam("wheels/r_radius", r_wheel_radius) ||
      !gnh.getParam("wheels/l_radius", l_wheel_radius) ||
      !gnh.getParam("wheels/wheel_base_diameter", wheel_base_diameter) ||
      !gnh.getParam("wheels/tics_per_revolution", tics_per_rev) ||
      !pnh.getParam("feed_forward/m", feed_fwd_m) ||
      !pnh.getParam("feed_forward/b", feed_fwd_b) ||
      !pnh.getParam("motor_bounds/min", motor_min) ||
      !pnh.getParam("motor_bounds/max", motor_max) ||
      !pnh.getParam("acceleration_threshold", acceleration_threshold)) {
    ROS_FATAL("ArduinoIO::ArduinoIO(): failed to load robot parameters");
    exit(EXIT_FAILURE);
  }

  //
  // initialize dynamic reconfigure server
  //

  auto cfg_fcn = bind(&ArduinoIO::sendCtrlParams, this, _1, _2, &cfg_init);
  server.reset(new ReconfigServer(ros::NodeHandle("~/PID")));
  server->setCallback(cfg_fcn);
  ros::Rate loop_rate(100);
  while (!cfg_init) loop_rate.sleep();

  //
  // initialize ros publishers and subscribers
  //

  tics_pub = nh.advertise<robot_io_msgs::Encoders>("tics", 30);
  est_vel_pub = nh.advertise<geometry_msgs::Twist>("est_vel", 30);
  motor_cmd_sub = nh.subscribe("motor_cmds", 30, &ArduinoIO::sendMotorCmds, this);
  vel_cmd_sub = nh.subscribe("vel_cmds", 30, &ArduinoIO::sendVelocityCmds, this);
}

ArduinoIO::~ArduinoIO()
{
  // stop the robot
  geometry_msgs::Twist stop_cmd;
  stop_cmd.linear.x = 0.0;
  stop_cmd.angular.z = 0.0;
  geometry_msgs::Twist::ConstPtr cmd_ptr(new geometry_msgs::Twist(stop_cmd));
  sendVelocityCmds(cmd_ptr);

  close(fd);
}

int ArduinoIO::initSerialPort(const char* port, int rate)
{
  // open serial port for reading and writing, don't make it the controlling 
  // terminal, and wait for the arduino to be ready before returning
  fd = open(port, O_RDWR | O_NOCTTY);
  if (fd == -1) {
    ROS_FATAL("Unable to open serial port %s", port);
    return -1;
  }

  // retrieve current terminal settings and save a copy
  struct termios newio;
  if (tcgetattr(fd, &newio) < 0) {
    ROS_FATAL("Unable to retrieve settings for port %s", port);
    return -1;
  }
  
  /* control options */
  newio.c_cflag |= CREAD | CLOCAL; // enable reading & ignore control lines
  newio.c_cflag &= ~CRTSCTS; // no hardware flow control

  // 8N1 input
  newio.c_cflag &= ~PARENB;  // no parity bit
  newio.c_cflag &= ~CSTOPB;  // 1 stop bit
  newio.c_cflag &= ~CSIZE;   // clear data bit size
  newio.c_cflag |= CS8;      // 8 data bits

  /* local options */
  newio.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // raw input

  /* input options */
  newio.c_iflag &= ~(IXON | IXOFF | IXANY); // no software flow control

  /* output options */
  newio.c_oflag &= ~OPOST; // raw output

  /* control character options */
  newio.c_cc[VMIN] = 0;
  newio.c_cc[VTIME] = 0;
  
  // set data rates
  speed_t brate;
  switch (rate) {
    case 4800:   brate=B4800;   break;
    case 9600:   brate=B9600;   break;
    case 19200:  brate=B19200;  break;
    case 38400:  brate=B38400;  break;
    case 57600:  brate=B57600;  break;
    case 115200: brate=B115200; break;
    default: ROS_WARN("Invalid baud_rate");
             ROS_WARN("Using default value of 57600");
             brate = B57600;
             break;
  }
  cfsetispeed(&newio, brate);
  cfsetospeed(&newio, brate);

  // set port attributes
  if (tcsetattr(fd, TCSANOW, &newio) < 0) {
    ROS_FATAL("Unable to configure attributes on port %s", port);
    return -1;
  }

  usleep(2000 * 1000);
  tcflush(fd, TCIOFLUSH);
  usleep(2000 * 1000); // pause to allow for arduino to reboot

  return 0;
}

void ArduinoIO::readArduinoMsg()
{
  static int failed_messages = 0;

  // read encoder tics
  union Long3 enc_tics;
  readBytes(enc_tics.c, sizeof(enc_tics));
  union Checksum check;
  readBytes(check.c, sizeof(check));
  if (check.i == fletcher16((uint8_t *)enc_tics.c, sizeof(enc_tics))) {
    robot_io_msgs::Encoders tics;
    tics.stamp = ros::Time::now();
    tics.left = enc_tics.l[0];
    tics.right = enc_tics.l[1];
    tics.millis = enc_tics.l[2];
    tics_pub.publish(tics);
    
    if (failed_messages > 30)
      ROS_INFO("ArduinoIO::readArduinoMsg(): recovered from communication "
          "failure");
    failed_messages = 0;
  } else {
    ++failed_messages;
  }

  // read estimated velocity
  union Float2 est_vel;
  readBytes(est_vel.c, sizeof(est_vel));
  readBytes(check.c, sizeof(check));
  if (check.i == fletcher16((uint8_t *)est_vel.c, sizeof(est_vel))) {
    geometry_msgs::Twist est_twist;
    est_twist.linear.x = est_vel.f[0];
    est_twist.angular.z = est_vel.f[1];
    est_vel_pub.publish(est_twist);

    if (failed_messages > 30)
      ROS_INFO("ArduinoIO::readArduinoMsg(): recovered from communication "
          "failure");
    failed_messages = 0;
  } else {
    ++failed_messages;
  }

  if (failed_messages > 30)
    ROS_WARN_THROTTLE(1.0, "ArduinoIO::readArduinoMsg(): more than 30 failed "
        "messages in succession");
}

void ArduinoIO::sendCtrlParams(Config& cfg, uint32_t level, bool* init)
{
  *init = true;

  union Float13 ctrl_params;
  ctrl_params.f[0] = cfg.kp;
  ctrl_params.f[1] = cfg.ki;
  ctrl_params.f[2] = cfg.kd;
  ctrl_params.f[3] = cfg.integral_threshold;
  ctrl_params.f[4] = motor_min;
  ctrl_params.f[5] = motor_max;
  ctrl_params.f[6] = acceleration_threshold;
  ctrl_params.f[7] = l_wheel_radius;
  ctrl_params.f[8] = r_wheel_radius;
  ctrl_params.f[9] = wheel_base_diameter;
  ctrl_params.f[10] = tics_per_rev;
  ctrl_params.f[11] = feed_fwd_m;
  ctrl_params.f[12] = feed_fwd_b;

  union Checksum check;
  check.i = fletcher16((uint8_t *)ctrl_params.c, sizeof(ctrl_params));

  char command_char = 'p';
  write(fd, &command_char, 1);
  write(fd, ctrl_params.c, sizeof(ctrl_params));
  write(fd, check.c, sizeof(check));

  /*
  ROS_INFO("sent ctrl params: kp = %.3f, ki = %.3f, kd = %.3f, it = %.3f, "
      "mmin = %.3f, mmax = %.3f, at = %.3f, cs = %d", ctrl_params.f[0], 
      ctrl_params.f[1], ctrl_params.f[2], ctrl_params.f[3], ctrl_params.f[4],
      ctrl_params.f[5], ctrl_params.f[6], check.i);
      */
}

void ArduinoIO::sendVelocityCmds(const geometry_msgs::Twist::ConstPtr& msg)
{
  union Float2 vel_cmds;
  vel_cmds.f[0] = msg->linear.x;
  vel_cmds.f[1] = msg->angular.z;

  union Checksum check;
  check.i = fletcher16((uint8_t *)vel_cmds.c, sizeof(vel_cmds));

  char command_char = 'v';
  write(fd, &command_char, 1);
  write(fd, vel_cmds.c, sizeof(vel_cmds));
  write(fd, check.c, sizeof(check));
}

void ArduinoIO::sendMotorCmds(const robot_io_msgs::MotorCommands::ConstPtr& msg)
{
  union Int2 motor_cmds;
  motor_cmds.i[0] = msg->left;
  motor_cmds.i[1] = msg->right;

  union Checksum check;
  check.i = fletcher16((uint8_t *)motor_cmds.c, sizeof(motor_cmds));

  char command_char = 'm';
  write(fd, &command_char, 1);
  write(fd, motor_cmds.c, sizeof(motor_cmds));
  write(fd, check.c, sizeof(check));
}

void ArduinoIO::run()
{
  while (ros::ok()) {
    readArduinoMsg();
    ros::spinOnce();
  }
}
