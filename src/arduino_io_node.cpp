#include "robot_io/arduino_io.h"

int main(int argc, char** argv)
{
  ros::init(argc, argv, "arduino_io_node");
  ros::NodeHandle nh, pnh("~");

  ArduinoIO arduino_io(nh, pnh);
  arduino_io.run();

  return 0;
}
