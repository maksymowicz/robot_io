#include <ros/ros.h>
#include <tf2_ros/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/PoseStamped.h>

#include <string.h>

int main(int argc, char* argv[])
{
  ros::init(argc, argv, "pose_publisher");
  ros::NodeHandle nh, pnh("~");
  ros::Publisher pub = nh.advertise<geometry_msgs::PoseStamped>("pose", 10);
  
  std::string pose_frame, world_frame, publish_frame;
  if (!pnh.getParam("pose_frame", pose_frame) ||
      !pnh.getParam("world_frame", world_frame) ||
      !pnh.getParam("publish_frame", publish_frame)) {
    ROS_FATAL("main(...): unable to retrieve parameters from server");
    exit(EXIT_FAILURE);
  }

  tf2_ros::Buffer tfBuffer;
  tf2_ros::TransformListener tfListener(tfBuffer);

  ros::Rate loop_rate(10);
  int seq = 0;
  while (ros::ok()) {

    geometry_msgs::TransformStamped tfs;
    try {
      tfs = tfBuffer.lookupTransform(world_frame, pose_frame, ros::Time(0));
      geometry_msgs::PoseStamped pose;
      pose.header.seq = ++seq;
      pose.header.stamp = tfs.header.stamp;
      pose.header.frame_id = publish_frame;
      pose.pose.position.x = tfs.transform.translation.x;
      pose.pose.position.y = tfs.transform.translation.y;
      pose.pose.position.z = 0.0;
      pose.pose.orientation = tfs.transform.rotation;
      pub.publish(pose);
    } catch (tf2::TransformException &ex) {
      ROS_WARN_THROTTLE(1.0, "%s",ex.what());
    }

    loop_rate.sleep();
  }

  return 0;
}
